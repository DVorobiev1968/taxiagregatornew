# Краткое техническое задание:
<ol>
<li>Разработка микросервиса Агрегатора такси.</li>
<li>Сервис работает с использованием инфраструктуры интернет, на базе Spring Framework.</li>
<li>Демонстрируется общий интрефейс: от создания объектов, сущностей, до их начальной конфигурации с использованием context</li>
<li>За основу берется описание объектов с рессурса <a href="https://iway.ru/ngapidoc">iWay </a> </li>
<li>Тестовое задание размещается на хостинге (типа Heroku) имеющий общий доступ, с целью демонстрации работы. По готовности я направляю соответствующую ссылку.
В качестве БД используется MySQL.</li>
</ol>

## Backend реализован в виде сервисного приложения, реализующего следующую бизнес-логику:
<ol>
<li>Поиск такси из пункта А в пункт Б.</li>
<li>Предоставление пользователю ценового предложения от эконом до бизнес-класса автомобиля.</li>
<li>Хранение истории поездок.</li>
<li>Бронирование такси, с возможностью его отмены.</li>
<li>Оповещение пользователю о намеченной поездке.</li>
</ol>
<p>Поскольку речь идет о тестовом примере, то в качестве заглушки коннектора агрегатора используем тестовую БД с набором тестовых маршрутов.<br>
Frontend разрабатывать не требуется.
</p>

## Описание БД.
<p>
Таблицы имеют уникальный ключ (ID) для организации реляционной связи (один к одному, однин-ко-многим) отсортированы по ID.
</p>

### Таблица: Users.
<p>информация о зарегистрированном в сервисе водителе такси, 
с указанием характеристик транспортного средства. (пока в разработке)
<table>
    <tr>
        <th>Тип</th>
        <th>Наименование</th>
        <th>Описание</th>
    </tr>
    <tr>
        <td>int</td>
        <td>Id</td>
        <td>идентификатор пользователя (unique+autoincrement)</td>
    </tr>
    <tr>
        <td>String</td>
        <td>First_Name</td>
        <td>Имя</td>
    </tr>
    <tr>
        <td>String</td>
        <td>Last_Name</td>
        <td>Фамилия</td>
    </tr>
    <tr>
        <td>String</td>
        <td>Password</td>
        <td>пароль</td>
    </tr>
    <tr>
        <td>Binary</td>
        <td>Photo</td>
        <td>Фотография</td>
    </tr>
    <tr>
        <td>Object</td>
        <td>Avto</td>
        <td>Связная таблица Car (Relation ID) </td>
    </tr>

</table>

### Таблица: Car.
<p>информация характеристик транспортного средства (пока в разработке)
<table>
    <tr>
        <th>Тип</th>
        <th>Наименование</th>
        <th>Описание</th>
    </tr>
    <tr>
        <td>int</td>
        <td>Id</td>
        <td>идентификатор авто (unique+autoincrement)</td>
    </tr>
    <tr>
        <td>int</td>
        <td>User_Id</td>
        <td>идентификатор водителя (CONSTRAINT CONTACT_ID FOREIGN KEY (CONTACT_ID))</td>
    </tr>
    <tr>
        <td>int</td>
        <td>Type</td>
        <td>тип авто (1-стандарт, 2-комфорт, 3 -минивэн.)</td>
    </tr>
    <tr>
        <td>float</td>
        <td>Price</td>
        <td>цена за 1 км. Пути.</td>
    </tr>
    <tr>
        <td>String</td>
        <td>Model</td>
        <td>Марка, модель авто</td>
    </tr>

</table>

## Описание объектов

### Класс Passenger
<p>
Класс описывает клиента который заказывает такси
</p>

<table>
    <tr>
        <th>Тип</th>
        <th>Наименование</th>
        <th>Описание</th>
    </tr>
    <tr>
        <td>String</td>
        <td>name</td>
        <td>Имя пассажира</td>
    </tr>
    <tr>
        <td>String</td>
        <td>phone</td>
        <td>Контактный телефон</td>
    </tr>
    <tr>
        <td>String</td>
        <td>email</td>
        <td>Адрес электронной почты</td>
    </tr>
    <tr>
        <td>int</td>
        <td>companionCount</td>
        <td>Кол-во пассажиров попутчиков</td>
    </tr>
</table>

```
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    @Column(name = "id", nullable = false)
    private Long id;

    @Basic
    @Column(name = "name")
    private String name;

    @Basic
    @Column(name = "phone")
    private String phone;

    @Basic
    @Column(name = "email")
    private String email;

    @Basic
    @Column(name = "companion_сount")
    private int companion_сount;
```

### Класс Address
<p>
Класс локации определяет адрес для локации клиента
</p>

<table>
    <th>
        <th>Тип</th>
        <th>Наименование</th>
        <th>Описание</th>
    </th>
    <tr>
        <td>String</td>
        <td>City</td>
        <td>Город </td>
    </tr>
    <tr>
        <td>String</td>
        <td>Street</td>
        <td>улица </td>
    </tr>
    <tr>
        <td>int</td>
        <td>house</td>
        <td>Номер дома </td>
    </tr>
    <tr>
        <td>int</td>
        <td>entrance</td>
        <td>Номер подъезда </td>
    </tr>
    <tr>
        <td>String</td>
        <td>coordinates</td>
        <td>Координаты для Яндекс-карт </td>
    </tr>
</table>

```
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    @Column(name = "id", nullable = false)
    private Long id;

    @Basic
    @Column(name = "city")
    private String city;

    @Basic
    @Column(name = "street")
    private String street;

    @Basic
    @Column(name = "house")
    private int house;

    @Basic
    @Column(name = "entrance")
    private int entrance;

    @Basic
    @Column(name = "coordinates")
    private String coordinates;

```
###  Класс Train
<p>
Класс описывает информацию для бронирования поездки, 
когда пассажира необходимо забрать с жд вокзала. </p>
<table>
    <th>
        <th>Тип</th>
        <th>Наименование</th>
        <th>Описание</th>
    </th>
    <tr>
        <td>String</td>
        <td>name</td>
        <td>Наименование рейса</td>
    </tr>
    <tr>
        <td>String</td>
        <td>number</td>
        <td>Номер рейса</td>
    </tr>
    <tr>
        <td>int</td>
        <td>carriage</td>
        <td>Номер вагоне</td>
    </tr>
    <tr>
        <td>Date</td>
        <td>date</td>
        <td>Дата и время прибытия (UTC+0)</td>
    </tr>

</table>

```
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Long id;

    @Basic
    @Column(name = "number")
    private String number;

    @Basic
    @Column(name = "name")
    private String name;

    @Basic
    @Column(name = "carriage")
    private int carriage;

    @Basic
    @Column(name = "date")
    private Date date;

```


###  Класс Trip
<p>
Класс описывает объект поездки. </p>
<table>
    <tr>
        <th>Тип</th>
        <th>Наименование</th>
        <th>Описание</th>
    </tr>
    <tr>
        <td>String</td>
        <td>name</td>
        <td>Наименование поездки</td>
    </tr>
    <tr>
        <td>float</td>
        <td>price</td>
        <td>Стоимость поездки</td>
    </tr>
    <tr>
        <td>String</td>
        <td>comment</td>
        <td>Комментарии заказчика для водителя</td>
    </tr>
    <tr>
        <td>float</td>
        <td>price</td>
        <td>Стоимость поездки</td>
    </tr>
    <tr>
        <td>Address</td>
        <td>finishLocation</td>
        <td>Объект описывающий окончание поездки</td>
    </tr>
    <tr>
        <td>Train</td>
        <td>startLocation</td>
        <td>Объект описывающий окончание поездки</td>
    </tr>
    <tr>
        <td>Passenger</td>
        <td>passenger</td>
        <td>Объект описывающий пассажиров</td>
    </tr>
</table>

```
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Long id;

    @Basic
    @Column(name = "name")
    private String name;

    @Basic
    @Column(name = "price")
    private float price;

    @Basic
    @Column(name = "comment")
    private String comment;

    @ManyToOne
    @JoinColumn(name = "address_id", referencedColumnName = "id")
    private Address address;

    @ManyToOne
    @JoinColumn(name = "train_id", referencedColumnName = "id")
    private Train train;

    @ManyToOne
    @JoinColumn(name = "passenger_id", referencedColumnName = "id")
    private Passenger passenger;
```