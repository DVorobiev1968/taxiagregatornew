package ru.DVorobiev.Taxi.agregator.Entity;

import lombok.*;

import javax.persistence.*;
import java.util.Date;
import java.util.Random;

@Entity
@EqualsAndHashCode
@NoArgsConstructor
@Setter
@Getter
public class Train {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Long id;

    @Basic
    @Column(name = "number")
    private String number;

    @Basic
    @Column(name = "name")
    private String name;

    @Basic
    @Column(name = "carriage")
    private int carriage;

    @Basic
    @Column(name = "date")
    private Date date;

//    public Train() {
//        Random random = new Random();
//        this.number=String.format("passage_%d",Math.abs(random.nextInt(1000)));
//        this.name=String.format("Route_%d",Math.abs(random.nextInt(1000)));
//        this.carriage=Math.abs(random.nextInt(20));
//        this.date.setDate(Math.abs(random.nextInt(28)));
//    }

    public Train(String number, String name,boolean typeRandom) {
        this.number = number;
        this.name = name;
        this.date=new Date();
        if (typeRandom){
            Random random = new Random();
            this.number=String.format("passage_%d",Math.abs(random.nextInt(1000)));
            this.name=String.format("Route_%d",Math.abs(random.nextInt(1000)));
            this.carriage=Math.abs(random.nextInt(20));
            this.date.setDate(Math.abs(random.nextInt(28)));
        }
    }

    @Override
    public String toString(){
        return String.format("Train[id=%d, number=%s, name=%s, carriage=%s, date=%s",
                this.id,this.number,this.name,this.carriage,this.date.toString());
    }
}
