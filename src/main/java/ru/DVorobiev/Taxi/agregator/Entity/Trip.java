package ru.DVorobiev.Taxi.agregator.Entity;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.Random;

@Entity
@NoArgsConstructor
@Setter
@Getter
@EqualsAndHashCode
public class Trip {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Long id;

    @Basic
    @Column(name = "name")
    private String name;

    @Basic
    @Column(name = "price")
    private float price;

    @Basic
    @Column(name = "comment")
    private String comment;

    @ManyToOne
    @JoinColumn(name = "address_id", referencedColumnName = "id")
    private Address address;

    @ManyToOne
    @JoinColumn(name = "train_id", referencedColumnName = "id")
    private Train train;

    @ManyToOne
    @JoinColumn(name = "passenger_id", referencedColumnName = "id")
    private Passenger passenger;

    @Override
    public String toString(){
        return String.format("Trip[id=%d, name=%s, price=3.2f, comment=%s, %s, %s, %s",
                this.id,this.name, this.price,this.comment,
                this.address.toString(),
                this.train.toString(),
                this.passenger.toString());

    }

    public Trip(String name) {
        Random random=new Random();
        this.name = name;
        this.price = 0.0f;
        this.comment = String.format("testing trip N-%d",Math.abs(random.nextInt(100)));
        this.address=new Address("Perm",true);
        this.train=new Train("","",true);
        this.passenger=new Passenger("Sidorov Pavel Petrovich",true);
    }
}
