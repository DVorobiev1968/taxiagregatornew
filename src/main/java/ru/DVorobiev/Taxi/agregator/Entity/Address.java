package ru.DVorobiev.Taxi.agregator.Entity;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.Random;

@Entity
@EqualsAndHashCode
@NoArgsConstructor
@Setter
@Getter
public class Address {
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    @Column(name = "id", nullable = false)
    private Long id;

    @Basic
    @Column(name = "city")
    private String city;

    @Basic
    @Column(name = "street")
    private String street;

    @Basic
    @Column(name = "house")
    private int house;

    @Basic
    @Column(name = "entrance")
    private int entrance;

    @Basic
    @Column(name = "coordinates")
    private String coordinates;

    private String setCoordinate(){
        Random random=new Random();
        return String.format("59:%d;59:%d",Math.abs(random.nextInt(10000)),
                Math.abs(random.nextInt(20000)));
    }

    public Address(String city,boolean typeRandom) {
        this.city = city;
        if (typeRandom) {
            Random random = new Random();
//            this.id = (long) Math.abs(random.nextInt(10));
            this.coordinates=setCoordinate();
            this.house=Math.abs(random.nextInt(10));
            this.entrance=Math.abs(random.nextInt(20));
            this.street=String.format("Street_%d", Math.abs(random.nextInt(100)));
        }
    }

    @Override
    public String toString() {
        return String.format("Address[id=%d, city='%s', street='%s', house=%d, entrance=%d, coordinates='%s']",
                id, city, street, house, entrance, coordinates);
    }

}
