package ru.DVorobiev.Taxi.agregator.Entity;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.Random;

@Entity
@EqualsAndHashCode
@NoArgsConstructor
@Setter
@Getter
public class Passenger {
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    @Column(name = "id", nullable = false)
    private Long id;

    @Basic
    @Column(name = "name")
    private String name;

    @Basic
    @Column(name = "phone")
    private String phone;

    @Basic
    @Column(name = "email")
    private String email;

    @Basic
    @Column(name = "companion_сount")
    private int companion_сount;

    @Override
    public String toString(){
        return String.format("Passenger[id=%d, name=%s, phone=%s, email=%s, companion=%d]",
                id,name,phone,email,companion_сount);
    }

    public Passenger(String name,boolean typeRandom) {
        this.name = name;
        if (typeRandom) {
            Random random = new Random();
//            this.name=String.format("Passenger_%d", Math.abs(random.nextInt(100)));
            this.phone=String.format("+7(912)-%d-%d-%d",100+Math.abs(random.nextInt(100)),
                    300+Math.abs(random.nextInt(100)),
                    900+Math.abs(random.nextInt(10)));
            this.email=String.format("%s@example.com",this.name.substring(0,4).trim());
            this.companion_сount=Math.abs(random.nextInt(3));
        }
    }
}
