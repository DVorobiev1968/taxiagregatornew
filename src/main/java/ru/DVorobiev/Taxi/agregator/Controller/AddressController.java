package ru.DVorobiev.Taxi.agregator.Controller;

import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import ru.DVorobiev.Taxi.agregator.Entity.Address;
import ru.DVorobiev.Taxi.agregator.search.AddressSearchValues;
import ru.DVorobiev.Taxi.agregator.service.AddressService;
import ru.DVorobiev.Taxi.agregator.util.MyLogger;

import java.util.List;
import java.util.NoSuchElementException;

@RestController
@RequestMapping("/address")
@CrossOrigin(origins = "http://localhost:4200")
public class AddressController {
    private final AddressService addressService;

    public AddressController(AddressService addressService) {
        this.addressService = addressService;
    }

    @GetMapping(value="/form")
    public String addressForm(Model model){
        model.addAttribute("addr",new Address("Perm",true));
        return "addressForm";
    }

    @RequestMapping(value="/form",method=RequestMethod.POST)
    public String addressFormSubmit(@ModelAttribute Address address, Model model){
        model.addAttribute("addr",new Address("Perm",true));
        return "addressForm";
    }

    @GetMapping("/all")
    public List<Address> findAll() {

        MyLogger.showMethodName("AddressController: findAll() ---------------------------------------------------------- ");
        return addressService.findAllByOrderByCityAsc();
    }


    @PostMapping("/add")
    public ResponseEntity<Address> add(@RequestBody Address address) {
        MyLogger.showMethodName("AddressController: add() ---------------------------------------------------------- ");
        if (address.getId() != null && address.getId() != 0) {
            return new ResponseEntity("redundant param: id MUST be null", HttpStatus.NOT_ACCEPTABLE);
        }

        if (address.getCity() == null || address.getCity().trim().length() == 0) {
            return new ResponseEntity("missed param: City", HttpStatus.NOT_ACCEPTABLE);
        }
        return ResponseEntity.ok(addressService.add(address));
    }

    @PutMapping("/update/{id}")
    public ResponseEntity update(@RequestBody Address address) {

        MyLogger.showMethodName("AddressController: update() ---------------------------------------------------------- ");

        if (address.getId() == null || address.getId() == 0) {
            return new ResponseEntity("missed param: id", HttpStatus.NOT_ACCEPTABLE);
        }

        // если передали пустое значение title
        if (address.getCity() == null || address.getCity().trim().length() == 0) {
            return new ResponseEntity("missed param: title", HttpStatus.NOT_ACCEPTABLE);
        }

        // save работает как на добавление, так и на обновление
        addressService.update(address);

        return new ResponseEntity(HttpStatus.OK); // просто отправляем статус 200 (операция прошла успешно)
    }

    // параметр id передаются не в BODY запроса, а в самом URL
    @GetMapping("/id/{id}")
    public ResponseEntity<Address> findById(@PathVariable Long id) {

        MyLogger.showMethodName("AddressController: findById() ---------------------------------------------------------- ");
        Address address = null;

        try {
            address = addressService.findById(id);
        } catch (NoSuchElementException e) {
            e.printStackTrace();
            return new ResponseEntity("id=" + id + " not found", HttpStatus.NOT_ACCEPTABLE);
        }

        return ResponseEntity.ok(address);
    }

    @DeleteMapping("/delete/{id}")
    public ResponseEntity delete(@PathVariable Long id) {

        MyLogger.showMethodName("AddressController: delete() ---------------------------------------------------------- ");

        try {
            addressService.deleteById(id);
        } catch (EmptyResultDataAccessException e) {
            e.printStackTrace();
            return new ResponseEntity("id=" + id + " not found", HttpStatus.NOT_ACCEPTABLE);
        }

        return new ResponseEntity(HttpStatus.OK);
    }

    @PostMapping("/search")
    public ResponseEntity<List<Address>> search(@RequestBody AddressSearchValues addressSearchValues) {

        MyLogger.showMethodName("AddressController: search() ---------------------------------------------------------- ");

        // если вместо текста будет пусто или null - вернутся все категории
        return ResponseEntity.ok(addressService.findByCity(addressSearchValues.getCity()));
    }

}
