package ru.DVorobiev.Taxi.agregator.Controller;

import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.DVorobiev.Taxi.agregator.Entity.Trip;
import ru.DVorobiev.Taxi.agregator.search.TripSearchValues;
import ru.DVorobiev.Taxi.agregator.service.TripService;
import ru.DVorobiev.Taxi.agregator.util.MyLogger;

import java.util.List;
import java.util.NoSuchElementException;

@RestController
@RequestMapping("/trip") // базовый адрес
// разрешить для этого рессурса получать данные с бэкенда
@CrossOrigin(origins = "http://localhost:4200")
public class TripController {

    private final TripService tripService; // сервис для доступа к данным (напрямую к репозиториям не обращаемся)


    // автоматическое внедрение экземпляра класса через конструктор
    // не используем @Autowired ля переменной класса, т.к. "Field injection is not recommended "
    public TripController(TripService tripService) {
        this.tripService = tripService;
    }


    // получение всех данных
    @GetMapping("/all")
    public ResponseEntity<List<Trip>> findAll() {

        MyLogger.showMethodName("trip: findAll() ---------------------------------------------------------------- ");

        return ResponseEntity.ok(tripService.findAll());
    }

    // добавление
    @PostMapping("/add")
    public ResponseEntity<Trip> add(@RequestBody Trip trip) {

        MyLogger.showMethodName("trip: add() ---------------------------------------------------------------- ");

        // проверка на обязательные параметры
        if (trip.getId() != null && trip.getId() != 0) {
            // id создается автоматически в БД (autoincrement), поэтому его передавать не нужно, иначе может быть конфликт уникальности значения
            return new ResponseEntity("redundant param: id MUST be null", HttpStatus.NOT_ACCEPTABLE);
        }

        // если передали пустое значение title
        if (trip.getName() == null || trip.getName().trim().length() == 0) {
            return new ResponseEntity("missed param: title", HttpStatus.NOT_ACCEPTABLE);
        }

        return ResponseEntity.ok(tripService.add(trip)); // возвращаем созданный объект со сгенерированным id

    }


    // обновление
    @PutMapping("/update/{id}")
    public ResponseEntity<Trip> update(@RequestBody Trip trip) {

        MyLogger.showMethodName("trip: update() ---------------------------------------------------------------- ");

        // проверка на обязательные параметры
        if (trip.getId() == null || trip.getId() == 0) {
            return new ResponseEntity("missed param: id", HttpStatus.NOT_ACCEPTABLE);
        }

        // если передали пустое значение title
        if (trip.getName() == null || trip.getName().trim().length() == 0) {
            return new ResponseEntity("missed param: title", HttpStatus.NOT_ACCEPTABLE);
        }


        // save работает как на добавление, так и на обновление
        tripService.update(trip);

        return new ResponseEntity(HttpStatus.OK); // просто отправляем статус 200 (операция прошла успешно)

    }


    // удаление по id
    // параметр id передаются не в BODY запроса, а в самом URL
    @DeleteMapping("/delete/{id}")
    public ResponseEntity delete(@PathVariable Long id) {

        MyLogger.showMethodName("trip: delete() ---------------------------------------------------------------- ");


        // можно обойтись и без try-catch, тогда будет возвращаться полная ошибка (stacktrace)
        // здесь показан пример, как можно обрабатывать исключение и отправлять свой текст/статус
        try {
            tripService.deleteById(id);
        } catch (EmptyResultDataAccessException e) {
            e.printStackTrace();
            return new ResponseEntity("id=" + id + " not found", HttpStatus.NOT_ACCEPTABLE);
        }
        return new ResponseEntity(HttpStatus.OK); // просто отправляем статус 200 (операция прошла успешно)
    }


    // получение объекта по id
    @GetMapping("/id/{id}")
    public ResponseEntity<Trip> findById(@PathVariable Long id) {

        MyLogger.showMethodName("trip: findById() ---------------------------------------------------------------- ");

        Trip trip = null;

        // можно обойтись и без try-catch, тогда будет возвращаться полная ошибка (stacktrace)
        // здесь показан пример, как можно обрабатывать исключение и отправлять свой текст/статус
        try {
            trip = tripService.findById(id);
        } catch (NoSuchElementException e) { // если объект не будет найден
            e.printStackTrace();
            return new ResponseEntity("id=" + id + " not found", HttpStatus.NOT_ACCEPTABLE);
        }

        return ResponseEntity.ok(trip);
    }

    // поиск по любым параметрам
    // TripSearchValues содержит все возможные параметры поиска
    @PostMapping("/search")
    public ResponseEntity<Page<Trip>> search(@RequestBody TripSearchValues tripSearchValues) {
        MyLogger.showMethodName("trip: search() ---------------------------------------------------------------- ");
        String name = tripSearchValues.getName() != null ? tripSearchValues.getName() : null;
        Long addressId = tripSearchValues.getAddressId() != null ? tripSearchValues.getAddressId() : null;
        Long trainId = tripSearchValues.getTrainId() != null ? tripSearchValues.getTrainId() : null;
        Long passengerId = tripSearchValues.getPassengerId() != null ? tripSearchValues.getPassengerId() : null;

        String sortColumn = tripSearchValues.getSortColumn() != null ? tripSearchValues.getSortColumn() : null;
        String sortDirection = tripSearchValues.getSortDirection() != null ? tripSearchValues.getSortDirection() : null;

        Integer pageNumber = tripSearchValues.getPageNumber() != null ? tripSearchValues.getPageNumber() : null;
        Integer pageSize = tripSearchValues.getPageSize() != null ? tripSearchValues.getPageSize() : null;

        Sort.Direction direction = sortDirection == null || sortDirection.trim().length() == 0 || sortDirection.trim().equals("asc") ? Sort.Direction.ASC : Sort.Direction.DESC;

        Sort sort = Sort.by(direction, sortColumn);

        PageRequest pageRequest = PageRequest.of(pageNumber, pageSize, sort);
        Page result = tripService.findByParams(name, addressId, trainId, passengerId, pageRequest);

        return ResponseEntity.ok(result);
    }

}
