package ru.DVorobiev.Taxi.agregator.Controller;

import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.DVorobiev.Taxi.agregator.Entity.Passenger;
import ru.DVorobiev.Taxi.agregator.search.PassengerSearchValues;
import ru.DVorobiev.Taxi.agregator.service.PassengerService;
import ru.DVorobiev.Taxi.agregator.util.MyLogger;

import java.util.List;
import java.util.NoSuchElementException;

@RestController
@RequestMapping("/passenger")
@CrossOrigin(origins = "http://localhost:4200")
public class PassengerController {
    private PassengerService passengerService;

    public PassengerController(PassengerService passengerService) {
        this.passengerService = passengerService;
    }

    @GetMapping("/all")
    public List<Passenger> findAll() {

        MyLogger.showMethodName("PassengerController: findAll() ---------------------------------------------------------- ");
        return passengerService.findAllByOrderByNameAsc();
    }


    @PostMapping("/add")
    public ResponseEntity<Passenger> add(@RequestBody Passenger passenger) {
        MyLogger.showMethodName("PassengerController: add() ---------------------------------------------------------- ");
        if (passenger.getId() != null && passenger.getId() != 0) {
            return new ResponseEntity("redundant param: id MUST be null", HttpStatus.NOT_ACCEPTABLE);
        }

        if (passenger.getName() == null || passenger.getName().trim().length() == 0) {
            return new ResponseEntity("missed param: Name", HttpStatus.NOT_ACCEPTABLE);
        }
        return ResponseEntity.ok(passengerService.add(passenger));
    }

    @PutMapping("/update/{id}")
    public ResponseEntity update(@RequestBody Passenger passenger) {
        MyLogger.showMethodName("PassengerController: update() ---------------------------------------------------------- ");
        if (passenger.getId() == null || passenger.getId() == 0) {
            return new ResponseEntity("missed param: id", HttpStatus.NOT_ACCEPTABLE);
        }
        // если передали пустое значение title
        if (passenger.getName() == null || passenger.getName().trim().length() == 0) {
            return new ResponseEntity("missed param: title", HttpStatus.NOT_ACCEPTABLE);
        }

        // save работает как на добавление, так и на обновление
        passengerService.update(passenger);

        return new ResponseEntity(HttpStatus.OK); // просто отправляем статус 200 (операция прошла успешно)
    }

    // параметр id передаются не в BODY запроса, а в самом URL
    @GetMapping("/id/{id}")
    public ResponseEntity<Passenger> findById(@PathVariable Long id) {
        MyLogger.showMethodName("PassengerController: findById() ---------------------------------------------------------- ");
        Passenger passenger = null;
        try {
            passenger = passengerService.findById(id);
        } catch (NoSuchElementException e) {
            e.printStackTrace();
            return new ResponseEntity("id=" + id + " not found", HttpStatus.NOT_ACCEPTABLE);
        }
        return ResponseEntity.ok(passenger);
    }

    @DeleteMapping("/delete/{id}")
    public ResponseEntity delete(@PathVariable Long id) {
        MyLogger.showMethodName("PassengerController: delete() ---------------------------------------------------------- ");
        try {
            passengerService.deleteById(id);
        } catch (EmptyResultDataAccessException e) {
            e.printStackTrace();
            return new ResponseEntity("id=" + id + " not found", HttpStatus.NOT_ACCEPTABLE);
        }
        return new ResponseEntity(HttpStatus.OK);
    }

    @PostMapping("/search")
    public ResponseEntity<List<Passenger>> search(@RequestBody PassengerSearchValues passengerSearchValues) {
        MyLogger.showMethodName("PassengerController: search() ---------------------------------------------------------- ");
        // если вместо текста будет пусто или null - вернутся все категории
        return ResponseEntity.ok(passengerService.findByName(passengerSearchValues.getName()));
    }
}
