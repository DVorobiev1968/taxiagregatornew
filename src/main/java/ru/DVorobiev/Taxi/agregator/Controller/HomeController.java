package ru.DVorobiev.Taxi.agregator.Controller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class HomeController {
    // inject via application.properties
    @Value("${welcome.message}")
    private String message;

    @GetMapping("/home")
    public String home(@RequestParam(name = "name", required = false,
            defaultValue = "Home Aggregator Page") String name, Model model) {
        model.addAttribute("message", name);

        return "index";     // return view
    }
}
