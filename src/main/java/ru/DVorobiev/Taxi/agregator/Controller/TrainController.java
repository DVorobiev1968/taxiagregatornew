package ru.DVorobiev.Taxi.agregator.Controller;

import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.DVorobiev.Taxi.agregator.Entity.Train;
import ru.DVorobiev.Taxi.agregator.search.TrainSearchValues;
import ru.DVorobiev.Taxi.agregator.service.TrainService;
import ru.DVorobiev.Taxi.agregator.util.MyLogger;

import java.util.List;
import java.util.NoSuchElementException;

@RestController
@RequestMapping("/train")
@CrossOrigin(origins = "http://localhost:4200")
public class TrainController {
    private TrainService trainService;

    public TrainController(TrainService trainService) {
        this.trainService = trainService;
    }


    @GetMapping("/all")
    public List<Train> findAll() {

        MyLogger.showMethodName("TrainController: findAll() ---------------------------------------------------------- ");
        return trainService.findAllByOrderByNameAsc();
    }


    @PostMapping("/add")
    public ResponseEntity<Train> add(@RequestBody Train train) {
        MyLogger.showMethodName("TrainController: add() ---------------------------------------------------------- ");
        if (train.getId() != null && train.getId() != 0) {
            return new ResponseEntity("redundant param: id MUST be null", HttpStatus.NOT_ACCEPTABLE);
        }

        if (train.getName() == null || train.getName().trim().length() == 0) {
            return new ResponseEntity("missed param: City", HttpStatus.NOT_ACCEPTABLE);
        }


        return ResponseEntity.ok(trainService.add(train));
    }

    @PutMapping("/update/{id}")
    public ResponseEntity update(@RequestBody Train train) {

        MyLogger.showMethodName("TrainController: update() ---------------------------------------------------------- ");

        if (train.getId() == null || train.getId() == 0) {
            return new ResponseEntity("missed param: id", HttpStatus.NOT_ACCEPTABLE);
        }

        // если передали пустое значение title
        if (train.getName() == null || train.getName().trim().length() == 0) {
            return new ResponseEntity("missed param: title", HttpStatus.NOT_ACCEPTABLE);
        }

        // save работает как на добавление, так и на обновление
        trainService.update(train);

        return new ResponseEntity(HttpStatus.OK); // просто отправляем статус 200 (операция прошла успешно)
    }

    // параметр id передаются не в BODY запроса, а в самом URL
    @GetMapping("/id/{id}")
    public ResponseEntity<Train> findById(@PathVariable Long id) {

        MyLogger.showMethodName("TrainController: findById() ---------------------------------------------------------- ");


        Train train = null;

        try {
            train = trainService.findById(id);
        } catch (NoSuchElementException e) {
            e.printStackTrace();
            return new ResponseEntity("id=" + id + " not found", HttpStatus.NOT_ACCEPTABLE);
        }

        return ResponseEntity.ok(train);
    }

    @DeleteMapping("/delete/{id}")
    public ResponseEntity delete(@PathVariable Long id) {

        MyLogger.showMethodName("TrainController: delete() ---------------------------------------------------------- ");

        try {
            trainService.deleteById(id);
        } catch (EmptyResultDataAccessException e) {
            e.printStackTrace();
            return new ResponseEntity("id=" + id + " not found", HttpStatus.NOT_ACCEPTABLE);
        }

        return new ResponseEntity(HttpStatus.OK);
    }

    @PostMapping("/search")
    public ResponseEntity<Train> search(@RequestBody TrainSearchValues trainSearchValues) {

        MyLogger.showMethodName("TrainController: search() ---------------------------------------------------------- ");

        // если вместо текста будет пусто или null - вернутся все категории
        return ResponseEntity.ok(trainService.findByName(trainSearchValues.getName()));
    }

}
