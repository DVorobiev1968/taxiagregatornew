package ru.DVorobiev.Taxi.agregator.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import ru.DVorobiev.Taxi.agregator.Entity.Trip;
import ru.DVorobiev.Taxi.agregator.repo.TripRepository;

import javax.transaction.Transactional;
import java.util.List;

@Service
@Transactional
public class TripService {
    private final TripRepository repository;

    public TripService(TripRepository repository) {
        this.repository = repository;
    }

    public List<Trip> findAll() {
        return repository.findAll();
    }

    public Trip add(Trip trip) {
        return repository.save(trip);
    }

    public Trip update(Trip trip) {
        return repository.save(trip);
    }

    public void deleteById(Long id) {
        repository.deleteById(id);
    }

    public Page findByParams(String name, Long addressId, Long trainId, Long passengerId, PageRequest paging) {
        return repository.findByParams(name, addressId, trainId, passengerId, paging);
    }

    public Trip findById(Long id) {
        return repository.findById(id).get();
    }
}
