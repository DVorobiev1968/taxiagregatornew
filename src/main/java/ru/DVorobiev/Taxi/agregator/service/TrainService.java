package ru.DVorobiev.Taxi.agregator.service;

import org.springframework.stereotype.Service;
import ru.DVorobiev.Taxi.agregator.Entity.Train;
import ru.DVorobiev.Taxi.agregator.repo.TrainRepository;

import javax.transaction.Transactional;
import java.util.List;

@Service
@Transactional
public class TrainService {
    private final TrainRepository repository;

    public TrainService(TrainRepository repository) {
        this.repository = repository;
    }

    public List<Train> findAll() {
        return repository.findAll();
    }

    public Train add(Train train) {
        return repository.save(train);
    }

    public Train update(Train train) {
        return repository.save(train);
    }

    public void deleteById(Long id) {
        repository.deleteById(id);
    }

    public Train findByNumber(int number) {
        return repository.findByNumber(number);
    }

    public Train findByName(String name) {
        return repository.findByName(name);
    }

    public Train findById(Long id) {
        return repository.findById(id).get();
    }

    public List<Train> findAllByOrderByNameAsc() {
        return repository.findAllByOrderByNameAsc();
    }
}
