package ru.DVorobiev.Taxi.agregator.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.stereotype.Service;
import ru.DVorobiev.Taxi.agregator.Entity.Address;
import ru.DVorobiev.Taxi.agregator.repo.AddressRepository;

import javax.transaction.Transactional;
import java.util.List;

@Service
@Transactional
@ComponentScan("ru.DVorobiev.Taxi.agregator.repo") // указывать явно не требуется, оставил для примера
public class AddressService{
    @Autowired  // указывать явно не требуется, оставил для примера
    private final AddressRepository repository;

    public AddressService(AddressRepository repository) {
        this.repository = repository;
    }

    public List<Address> findAll() {
        return repository.findAll();
    }

    public Address add(Address address) {
        return repository.save(address);
    }

    public Address update(Address address) {
        return repository.save(address);
    }

    public void deleteById(Long id) {
        repository.deleteById(id);
    }

    public List<Address> findByCity(String city) {
        return repository.findByCity(city);
    }

    public Address findById(Long id) {
        return repository.findById(id).get();
    }

    public List<Address> findAllByOrderByCityAsc(){
        return repository.findAllByOrderByCityAsc();
    }

}
