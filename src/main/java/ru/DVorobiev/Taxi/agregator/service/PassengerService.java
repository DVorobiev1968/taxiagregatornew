package ru.DVorobiev.Taxi.agregator.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.stereotype.Service;
import ru.DVorobiev.Taxi.agregator.Entity.Passenger;
import ru.DVorobiev.Taxi.agregator.repo.PassengerRepository;

import javax.transaction.Transactional;
import java.util.List;

@Service
@Transactional
public class PassengerService {
    private final PassengerRepository repository;

    public PassengerService(PassengerRepository repository) {
        this.repository = repository;
    }

    public List<Passenger> findAll() {
        return repository.findAll();
    }

    public Passenger add(Passenger passenger) {
        return repository.save(passenger);
    }

    public Passenger update(Passenger passenger) {
        return repository.save(passenger);
    }

    public void deleteById(Long id) {
        repository.deleteById(id);
    }

    public List<Passenger> findByName(String text) {
        return repository.findByName(text);
    }

    public Passenger findById(Long id) {
        return repository.findById(id).get();
    }

    public List<Passenger> findAllByOrderByNameAsc() {
        return repository.findAllByOrderByNameAsc();
    }

}
