package ru.DVorobiev.Taxi.agregator.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import ru.DVorobiev.Taxi.agregator.Entity.Address;

import java.util.List;

@Repository
public interface AddressRepository extends JpaRepository<Address,Long> {
    @Query("SELECT c FROM Address c where" +
            "(:city is null or :city='' or lower(c.city) like lower(concat('%', :city,'%')))  " +
            "order by c.city asc"
    )
    List<Address> findByCity(@Param("city") String city);
    List<Address> findAllByOrderByCityAsc();
}
