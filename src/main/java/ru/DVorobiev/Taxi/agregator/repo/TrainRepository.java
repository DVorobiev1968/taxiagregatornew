package ru.DVorobiev.Taxi.agregator.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import ru.DVorobiev.Taxi.agregator.Entity.Train;

import java.util.List;

@Repository
public interface TrainRepository extends JpaRepository<Train,Long> {
    Train findByNumber(@Param("number") int number);
    Train findByName(@Param("name") String name);
    List<Train> findAllByOrderByNameAsc();
}
