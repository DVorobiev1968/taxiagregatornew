package ru.DVorobiev.Taxi.agregator.repo;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import ru.DVorobiev.Taxi.agregator.Entity.Trip;

@Repository
public interface TripRepository extends JpaRepository<Trip,Long> {
    @Query("SELECT t FROM Trip t where " +
            "(:name is null or :name='' or lower(t.name) like lower(concat('%', :name,'%'))) and" +
            "(:addressId is null or t.address.id=:addressId) and " +
            "(:trainId is null or t.train.id=:trainId) and " +
            "(:passengerId is null or t.passenger.id=:passengerId)"
    )
    Page<Trip> findByParams(@Param("name") String name,
                            @Param("addressId") Long addressId,
                            @Param("trainId") Long trainId,
                            @Param("passengerId") Long passengerId,
                            Pageable pageable
    );

}
