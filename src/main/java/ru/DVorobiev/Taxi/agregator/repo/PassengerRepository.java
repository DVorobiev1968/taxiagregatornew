package ru.DVorobiev.Taxi.agregator.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import ru.DVorobiev.Taxi.agregator.Entity.Passenger;

import java.util.List;

@Repository
public interface PassengerRepository extends JpaRepository<Passenger,Long> {
    List<Passenger> findByName(@Param("name") String name);
    List<Passenger> findAllByOrderByNameAsc();

}
