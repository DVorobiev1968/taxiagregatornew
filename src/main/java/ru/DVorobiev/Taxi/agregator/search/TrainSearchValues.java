package ru.DVorobiev.Taxi.agregator.search;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class TrainSearchValues {
    private String number;
    private String name;

    public TrainSearchValues(String number, String name) {
        this.number = number;
        this.name = name;
    }
}
