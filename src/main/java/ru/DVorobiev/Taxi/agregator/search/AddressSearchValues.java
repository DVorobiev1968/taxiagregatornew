package ru.DVorobiev.Taxi.agregator.search;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class AddressSearchValues {
    private String city;

    public AddressSearchValues(String city) {
        this.city = city;
    }
}
