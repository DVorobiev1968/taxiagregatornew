package ru.DVorobiev.Taxi.agregator.search;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class PassengerSearchValues {
    private String name;

    public PassengerSearchValues(String name) {
        this.name = name;
    }
}
