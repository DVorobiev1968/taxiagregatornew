-- MySQL dump 10.13  Distrib 8.0.26, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: taxi_test
-- ------------------------------------------------------
-- Server version	8.0.26

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `address`
--

DROP TABLE IF EXISTS `address`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `address` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `city` varchar(45) NOT NULL,
  `street` varchar(45) NOT NULL,
  `house` int NOT NULL,
  `entrance` int NOT NULL,
  `coordinates` varchar(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `index_title` (`city`) /*!80000 INVISIBLE */
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `address`
--

LOCK TABLES `address` WRITE;
/*!40000 ALTER TABLE `address` DISABLE KEYS */;
INSERT INTO `address` VALUES (10,'Dobryanka','Street_22',5,5,'59:1365;59:10178'),(11,'Perm','Street_49',0,16,'59:5292;59:13580'),(13,'Perm','Street_35',6,11,'59:4500;59:327'),(14,'Perm','Street_30',8,19,'59:3770;59:4196'),(15,'Perm','Street_15',4,18,'59:2578;59:17299'),(16,'Perm','Street_23',9,9,'59:2164;59:7957'),(17,'Perm','Street_40',7,3,'59:8770;59:6132'),(18,'Perm','Street_57',3,6,'59:2027;59:4422'),(19,'Perm','Street_85',5,9,'59:2867;59:18652'),(20,'Perm','Street_94',8,7,'59:6283;59:3698'),(21,'Perm','Street_80',5,10,'59:3926;59:16813');
/*!40000 ALTER TABLE `address` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `passenger`
--

DROP TABLE IF EXISTS `passenger`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `passenger` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `name` varchar(30) NOT NULL,
  `phone` varchar(20) NOT NULL,
  `email` varchar(30) NOT NULL,
  `companion_сount` int NOT NULL,
  PRIMARY KEY (`id`),
  KEY `name` (`name`) /*!80000 INVISIBLE */
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `passenger`
--

LOCK TABLES `passenger` WRITE;
/*!40000 ALTER TABLE `passenger` DISABLE KEYS */;
INSERT INTO `passenger` VALUES (10,'Petrov Sergey','+7(912)-137-336-900','Petr@example.com',0),(11,'Krylova Irina','+7(912)-189-319-903','Kryl@example.com',2),(12,'Sevastyznova Olga','+7(912)-122-300-903','Seva@example.com',1),(14,'Sidorov Pavel Petrovich','+7(912)-145-310-905','Sido@example.com',0),(15,'Sidorov Pavel Petrovich','+7(912)-151-332-905','Sido@example.com',0),(16,'Sidorov Pavel Petrovich','+7(912)-176-329-903','Sido@example.com',2),(17,'Sidorov Pavel Petrovich','+7(912)-122-388-903','Sido@example.com',2),(18,'Sidorov Pavel Petrovich','+7(912)-102-365-904','Sido@example.com',2),(19,'Sidorov Pavel Petrovich','+7(912)-193-390-909','Sido@example.com',0),(20,'Sidorov Pavel Petrovich','+7(912)-153-385-903','Sido@example.com',0),(21,'Sidorov Pavel Petrovich','+7(912)-195-307-904','Sido@example.com',2);
/*!40000 ALTER TABLE `passenger` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `train`
--

DROP TABLE IF EXISTS `train`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `train` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `number` varchar(15) NOT NULL,
  `name` varchar(45) NOT NULL,
  `carriage` int NOT NULL,
  `date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `index_title` (`number`) /*!80000 INVISIBLE */
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `train`
--

LOCK TABLES `train` WRITE;
/*!40000 ALTER TABLE `train` DISABLE KEYS */;
INSERT INTO `train` VALUES (10,'2222R','Perm2-Yaroslavl',0,'2021-11-23 05:05:16'),(11,'passage_213','Route_503',12,'2021-11-25 05:05:16'),(13,'111R','Perm2-Moscow',0,'2021-11-23 05:05:16'),(14,'passage_502','Route_645',2,'2021-11-16 05:23:35'),(15,'passage_133','Route_408',6,'2021-11-11 05:29:18'),(16,'passage_561','Route_886',18,'2021-11-11 05:46:55'),(17,'passage_576','Route_476',9,'2021-11-21 05:51:19'),(18,'passage_258','Route_696',5,'2021-11-10 05:59:45'),(19,'passage_945','Route_35',12,'2021-11-16 06:02:39'),(20,'passage_945','Route_456',19,'2021-11-15 06:02:39'),(21,'passage_87','Route_584',8,'2021-11-06 06:03:46');
/*!40000 ALTER TABLE `train` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `trip`
--

DROP TABLE IF EXISTS `trip`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `trip` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  `price` float NOT NULL,
  `comment` varchar(30) DEFAULT NULL,
  `address_id` bigint DEFAULT NULL,
  `train_id` bigint DEFAULT NULL,
  `passenger_id` bigint DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_address_idx` (`address_id`),
  KEY `fk_train_idx` (`train_id`),
  KEY `fk_passenger_idx` (`passenger_id`),
  KEY `index_name` (`name`),
  KEY `index_price` (`price`),
  CONSTRAINT `fk_address` FOREIGN KEY (`address_id`) REFERENCES `address` (`id`) ON DELETE SET NULL ON UPDATE RESTRICT,
  CONSTRAINT `fk_passenger` FOREIGN KEY (`passenger_id`) REFERENCES `passenger` (`id`) ON DELETE SET NULL ON UPDATE RESTRICT,
  CONSTRAINT `fk_train` FOREIGN KEY (`train_id`) REFERENCES `train` (`id`) ON DELETE SET NULL ON UPDATE RESTRICT
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `trip`
--

LOCK TABLES `trip` WRITE;
/*!40000 ALTER TABLE `trip` DISABLE KEYS */;
INSERT INTO `trip` VALUES (15,'Trip for update',0,'testing trip N-2',14,14,14),(17,'Trip_2',0,'testing trip N-3',15,15,15),(18,'Trip_2',0,'testing trip N-19',16,16,16),(19,'Trip_2',0,'testing trip N-27',17,17,17),(20,'Trip_2',0,'testing trip N-27',17,17,17),(21,'Trip for update',0,'testing trip N-2',18,18,18),(22,'New update trip',0,'testing trip N-47',20,20,20);
/*!40000 ALTER TABLE `trip` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-11-23 11:09:35
