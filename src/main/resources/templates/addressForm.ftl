<#import "parts/common.ftl" as c>

<@c.page>
List of address

<table>
    <thead>
    <tr>
        <th>City</th>
        <th>Street</th>
        <th>House</th>
        <th>Entrance</th>
        <th>Coordinates</th>
        <th></th>
    </tr>
    </thead>
    <tbody>
    <#list addresses as address>
        <tr>
            <td>${address.city}</td>
            <td>${address.street}</td>
            <td>${address.house}</td>
            <td>${address.entrance}</td>
            <td>${address.coordinates}</td>
            <td><a href="/address/id/${address.id}">edit</a></td>
        </tr>
    </#list>
    </tbody>
</table>
</@c.page>
