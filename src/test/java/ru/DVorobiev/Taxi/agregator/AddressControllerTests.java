package ru.DVorobiev.Taxi.agregator;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.event.annotation.BeforeTestClass;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.web.servlet.MockMvc;
import ru.DVorobiev.Taxi.agregator.Controller.AddressController;
import ru.DVorobiev.Taxi.agregator.Entity.Address;
import ru.DVorobiev.Taxi.agregator.repo.AddressRepository;
import ru.DVorobiev.Taxi.agregator.search.AddressSearchValues;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static ru.DVorobiev.Taxi.agregator.util.RequestBuilder.postJson;

@Slf4j
@SpringBootTest
@AutoConfigureMockMvc
@TestPropertySource("classpath:application-test.properties")     // указываем местоположение конфигурации БД для теста
//@Sql(value = {"/sql/address_before.sql"}, executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD)
//@Sql(value = {"/sql/address_after.sql"}, executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD)

public class AddressControllerTests {
    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private AddressController addressController;

    @Autowired
    private AddressRepository addressRepository;

    @Autowired
    private ObjectMapper objectMapper;

    @BeforeTestClass
    public void setUp() {
    }

    @Test
    public void contextLoads() {
        assertThat(addressController).isNotNull();
    }

    @DisplayName("Test AddressController.update()...")
    @Test
    public void addressControllerUpdateTest() throws Exception {
        long id = createAddress("Perm_1").getId();
        // создаем новый адрес город Добрянка
        Address address = new Address("Dobryanka", true);
        address.setId(id);

        mockMvc.perform(
                        put("/address/update/{id}", id)
                                .content(objectMapper.writeValueAsString(address))
                                .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk());
    }

    @DisplayName("Test AddressController.delete()...")
    @Test
    public void addressControllerDeleteTest() throws Exception {
        Address deleteAddress = createAddress("Perm_2");
        mockMvc.perform(
                        delete("/address/delete/{id}", deleteAddress.getId()))
                .andDo(print())
                .andExpect(status().isOk());
    }

    @DisplayName("Test AddressController.findAll()...")
    @Test
    public void addressControllerFindAllTest() throws Exception {
        this.mockMvc.perform(get("/address/all"))
                .andDo(print())
                .andExpect(status().isOk());
    }

    private Address createAddress(String city) {
        Address address = new Address(city, true);
        return addressRepository.save(address);
    }

    @DisplayName("Test AddressController.add()...")
    @Test
    public void addAddressTest() throws Exception {
        Address address = new Address("Perm", true);
        address.setId(0L);
        mockMvc.perform(
                        post("/address/add")
                                .content(objectMapper.writeValueAsString(address))
                                .contentType(MediaType.APPLICATION_JSON)
                )
                .andDo(print())
                .andExpect(status().isOk());
    }


    @DisplayName("Test AddressController.findById()...")
    @Test
    public void addressControllerFindByIdTest() throws Exception {
        long id = createAddress("Perm").getId();
        this.mockMvc.perform(get("/address/id/{id}", id))
                .andDo(print())
                .andExpect(status().isOk());
    }

    @Test
    public void addressControllerFindByCityTest() throws Exception {
        AddressSearchValues addressSearchValues = new AddressSearchValues("Dobr");
        this.mockMvc.perform(postJson("/address/search", addressSearchValues))
                .andDo(print())
                .andExpect(status().isOk());
    }

}