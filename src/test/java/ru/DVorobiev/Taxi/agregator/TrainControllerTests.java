package ru.DVorobiev.Taxi.agregator;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.event.annotation.BeforeTestClass;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.web.servlet.MockMvc;
import ru.DVorobiev.Taxi.agregator.Controller.TrainController;
import ru.DVorobiev.Taxi.agregator.Entity.Train;
import ru.DVorobiev.Taxi.agregator.repo.TrainRepository;
import ru.DVorobiev.Taxi.agregator.search.TrainSearchValues;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static ru.DVorobiev.Taxi.agregator.util.RequestBuilder.postJson;

@Slf4j
@SpringBootTest
@AutoConfigureMockMvc
@TestPropertySource("classpath:application-test.properties")     // указываем местоположение конфигурации БД для теста
//@Sql(value = {"/sql/train_before.sql"}, executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD)
//@Sql(value = {"/sql/train_after.sql"}, executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD)

public class TrainControllerTests {
    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private TrainController trainController;

    @Autowired
    private TrainRepository trainRepository;

    @Autowired
    private ObjectMapper objectMapper;

    @BeforeTestClass
    public void setUp() {
    }

    @Test
    public void contextLoads() {
        assertThat(trainController).isNotNull();
    }

    @DisplayName("Test TrainController.update()...")
    @Test
    public void trainControllerUpdateTest() throws Exception {
        long id = createTrain("1112T","Yaroslavl-Perm2",false).getId();
        Train train = new Train("2222R","Perm2-Yaroslavl",false);
        train.setId(id);

        mockMvc.perform(
                        put("/train/update/{id}", id)
                                .content(objectMapper.writeValueAsString(train))
                                .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk());
    }

    @DisplayName("Test TrainController.delete()...")
    @Test
    public void trainControllerDeleteTest() throws Exception {
        Train deleteTrain = createTrain("155ET","Vladovostok-Perm2",false);
        mockMvc.perform(
                        delete("/train/delete/{id}", deleteTrain.getId()))
                .andDo(print())
                .andExpect(status().isOk());
    }

    @DisplayName("Test TrainController.findAll()...")
    @Test
    public void trainControllerFindAllTest() throws Exception {
        this.mockMvc.perform(get("/train/all"))
                .andDo(print())
                .andExpect(status().isOk());
    }

    private Train createTrain(String number,String name, boolean type) {
        Train train = new Train(number,name, type);
        return trainRepository.save(train);
    }

//    private Train createTrain() {
//        Train train = new Train();
//        return trainRepository.save(train);
//    }

    @DisplayName("Test TrainController.add()...")
    @Test
    public void addTrainTest() throws Exception {
        Train train = new Train("111R","Perm2-Moscow", false);
        train.setId(0L);
        mockMvc.perform(
                        post("/train/add")
                                .content(objectMapper.writeValueAsString(train))
                                .contentType(MediaType.APPLICATION_JSON)
                )
                .andDo(print())
                .andExpect(status().isOk());
    }


    @DisplayName("Test TrainController.findById()...")
    @Test
    public void trainControllerFindByIdTest() throws Exception {
        long id = createTrain("","",true).getId();
        this.mockMvc.perform(get("/train/id/{id}", id))
                .andDo(print())
                .andExpect(status().isOk());
    }

    @Test
    public void trainControllerFindByNameTest() throws Exception {
        TrainSearchValues trainSearchValues = new TrainSearchValues("111","train");
        this.mockMvc.perform(postJson("/train/search", trainSearchValues))
                .andDo(print())
                .andExpect(status().isOk());
    }

}