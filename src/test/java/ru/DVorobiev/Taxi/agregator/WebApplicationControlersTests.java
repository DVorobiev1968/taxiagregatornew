package ru.DVorobiev.Taxi.agregator;

import net.bytebuddy.implementation.bind.annotation.IgnoreForBinding;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.entity.ContentType;
import org.apache.http.impl.client.HttpClientBuilder;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.web.servlet.MockMvc;
import ru.DVorobiev.Taxi.agregator.Controller.AddressController;
import ru.DVorobiev.Taxi.agregator.Controller.PassengerController;
import ru.DVorobiev.Taxi.agregator.Controller.TrainController;
import ru.DVorobiev.Taxi.agregator.Controller.TripController;

import java.io.IOException;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
@TestPropertySource(locations = "classpath:application-test.properties")     // указываем местоположение конфигурации БД для теста
@ContextConfiguration
public class WebApplicationControlersTests {
    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private AddressController addressController;

    @Autowired
    private PassengerController passengerController;

    @Autowired
    private TrainController trainController;

    @Autowired
    private TripController tripController;

    @Test
    public void contextLoadsTest() throws Exception {
        assertThat(addressController).isNotNull();
        assertThat(passengerController).isNotNull();
        assertThat(trainController).isNotNull();
        assertThat(tripController).isNotNull();
    }

    private void getPathTest() throws IOException {
        HttpUriRequest request = new HttpGet("http://localhost:8091/address/all");
        HttpResponse response = HttpClientBuilder.create().build().execute( request );
        String mimeType = ContentType.getOrDefault(response.getEntity()).getMimeType();
        assertEquals("application/json",mimeType);
    }

    @Test
    public void adrressControllerFindAllTest() throws Exception {
        this.mockMvc.perform(get("http://localhost:8091/address/all"))
                .andDo(print())
                .andExpect(status().isOk());
    }

    @Test
    public void passengerControllerFindAllTest() throws Exception {
        this.mockMvc.perform(get("http://localhost:8091/passenger/all"))
                .andDo(print())
                .andExpect(status().isOk());
    }

    @Test
    public void trainControllerFindAllTest() throws Exception {
        this.mockMvc.perform(get("http://localhost:8091/train/all"))
                .andDo(print())
                .andExpect(status().isOk());
    }

    @Test
    public void tripControllerFindAllTest() throws Exception {
        this.mockMvc.perform(get("http://localhost:8091/trip/all"))
                .andDo(print())
                .andExpect(status().isOk());
    }
}