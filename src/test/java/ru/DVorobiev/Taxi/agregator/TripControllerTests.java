package ru.DVorobiev.Taxi.agregator;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.event.annotation.BeforeTestClass;
import org.springframework.test.web.servlet.MockMvc;
import ru.DVorobiev.Taxi.agregator.Controller.TripController;
import ru.DVorobiev.Taxi.agregator.Entity.Trip;
import ru.DVorobiev.Taxi.agregator.repo.AddressRepository;
import ru.DVorobiev.Taxi.agregator.repo.PassengerRepository;
import ru.DVorobiev.Taxi.agregator.repo.TrainRepository;
import ru.DVorobiev.Taxi.agregator.repo.TripRepository;
import ru.DVorobiev.Taxi.agregator.search.TripSearchValues;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static ru.DVorobiev.Taxi.agregator.util.RequestBuilder.postJson;

@Slf4j
@SpringBootTest
@AutoConfigureMockMvc
@TestPropertySource("classpath:application-test.properties")     // указываем местоположение конфигурации БД для теста
//@Sql(value = {"/sql/trip_before.sql"}, executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD)
//@Sql(value = {"/sql/trip_after.sql"}, executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD)

public class TripControllerTests {
    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private TripController tripController;

    @Autowired
    private TripRepository tripRepository;

    @Autowired
    private AddressRepository addressRepository;

    @Autowired
    private PassengerRepository passengerRepository;

    @Autowired
    private TrainRepository trainRepository;


    @Autowired
    private ObjectMapper objectMapper;

    @BeforeTestClass
    public void setUp() {
    }

    @Test
    public void contextLoads() {
        assertThat(tripController).isNotNull();
    }

    @DisplayName("Test TripController.update()...")
    @Test
    public void tripControllerUpdateTest() throws Exception {

        Trip trip = createTrip("Trip for update");
        Long id=trip.getId();
        Trip newTrip = new Trip("New update trip");
        addressRepository.save(newTrip.getAddress());
        trainRepository.save(newTrip.getTrain());
        passengerRepository.save(newTrip.getPassenger());
        newTrip.setId(id);

        mockMvc.perform(
                        put("/trip/update/{id}", id)
                                .content(objectMapper.writeValueAsString(newTrip))
                                .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk());
    }

    @DisplayName("Test TripController.delete()...")
    @Test
    public void tripControllerDeleteTest() throws Exception {
        Trip deleteTrip = createTrip("Delete trip");
        mockMvc.perform(
                        delete("/trip/delete/{id}", deleteTrip.getId()))
                .andDo(print())
                .andExpect(status().isOk());
    }

    @DisplayName("Test TripController.findAll()...")
    @Test
    public void tripControllerFindAllTest() throws Exception {
        this.mockMvc.perform(get("/trip/all"))
                .andDo(print())
                .andExpect(status().isOk());
    }

    private Trip createTrip(String name) {
        Trip trip = new Trip(name);
        addressRepository.save(trip.getAddress());
        trainRepository.save(trip.getTrain());
        passengerRepository.save(trip.getPassenger());
        return tripRepository.save(trip);
    }

    @DisplayName("Test TripController.add()...")
    @Test
    public void addTripTest() throws Exception {
        Trip trip = createTrip("Trip_2");
        trip.setId(0L);
        mockMvc.perform(
                        post("/trip/add")
                                .content(objectMapper.writeValueAsString(trip))
                                .contentType(MediaType.APPLICATION_JSON)
                )
                .andDo(print())
                .andExpect(status().isOk());
    }

    @DisplayName("Test TripController.findById()...")
    @Test
    public void tripControllerFindByIdTest() throws Exception {
        long id = createTrip("Trip_add_1").getId();
        this.mockMvc.perform(get("/trip/id/{id}", id))
                .andDo(print())
                .andExpect(status().isOk());
    }

    @DisplayName("Test TripController.search...")
    @Test
    public void searchTripTest() throws Exception {
        TripSearchValues tripSearchValues=new TripSearchValues();
        tripSearchValues.setName("Trip");
        tripSearchValues.setPageNumber(1);
        tripSearchValues.setPageSize(3);
        tripSearchValues.setSortColumn("name");
        tripSearchValues.setSortDirection("ASC");
        this.mockMvc.perform(postJson("/trip/search", tripSearchValues))
                .andDo(print())
                .andExpect(status().isOk());

    }
}