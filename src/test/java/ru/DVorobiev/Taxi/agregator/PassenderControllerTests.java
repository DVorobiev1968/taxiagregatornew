package ru.DVorobiev.Taxi.agregator;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.event.annotation.BeforeTestClass;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.web.servlet.MockMvc;
import ru.DVorobiev.Taxi.agregator.Controller.PassengerController;
import ru.DVorobiev.Taxi.agregator.Entity.Passenger;
import ru.DVorobiev.Taxi.agregator.repo.PassengerRepository;
import ru.DVorobiev.Taxi.agregator.search.PassengerSearchValues;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static ru.DVorobiev.Taxi.agregator.util.RequestBuilder.postJson;

@Slf4j
@SpringBootTest
@AutoConfigureMockMvc
@TestPropertySource("classpath:application-test.properties")     // указываем местоположение конфигурации БД для теста
//@Sql(value = {"/sql/passenger_before.sql"}, executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD)
//@Sql(value = {"/sql/passenger_after.sql"}, executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD)

public class PassenderControllerTests {
    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private PassengerController passengerController;

    @Autowired
    private PassengerRepository passengerRepository;

    @Autowired
    private ObjectMapper objectMapper;

    @BeforeTestClass
    public void setUp() {
    }

    @Test
    public void contextLoads() {
        assertThat(passengerController).isNotNull();
    }

    @DisplayName("Test PassengerController.update()...")
    @Test
    public void passengerControllerUpdateTest() throws Exception {
        long id = createPassenger("Ivanov Ivan Ivanovich").getId();
        Passenger passenger = new Passenger("Petrov Sergey", true);
        passenger.setId(id);

        mockMvc.perform(
                        put("/passenger/update/{id}", id)
                                .content(objectMapper.writeValueAsString(passenger))
                                .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk());
    }

    @DisplayName("Test PassengerController.delete()...")
    @Test
    public void passengerControllerDeleteTest() throws Exception {
        Passenger deletePassenger = createPassenger("Lapteva Natalya");
        mockMvc.perform(
                        delete("/passenger/delete/{id}", deletePassenger.getId()))
                .andDo(print())
                .andExpect(status().isOk());
    }

    @DisplayName("Test PassengerController.findAll()...")
    @Test
    public void passengerControllerFindAllTest() throws Exception {
        this.mockMvc.perform(get("/passenger/all"))
                .andDo(print())
                .andExpect(status().isOk());
    }

    private Passenger createPassenger(String name) {
        Passenger passenger = new Passenger(name, true);
        return passengerRepository.save(passenger);
    }

    @DisplayName("Test PassengerController.add()...")
    @Test
    public void addPassengerTest() throws Exception {
        Passenger passenger = new Passenger("Sevastyznova Olga", true);
        passenger.setId(0L);
        mockMvc.perform(
                        post("/passenger/add")
                                .content(objectMapper.writeValueAsString(passenger))
                                .contentType(MediaType.APPLICATION_JSON)
                )
                .andDo(print())
                .andExpect(status().isOk());
    }


    @DisplayName("Test PassengerController.findById()...")
    @Test
    public void passengerControllerFindByIdTest() throws Exception {
        long id = createPassenger("Krylova Irina").getId();
        this.mockMvc.perform(get("/passenger/id/{id}", id))
                .andDo(print())
                .andExpect(status().isOk());
    }

    @Test
    public void passengerControllerFindByNameTest() throws Exception {
        PassengerSearchValues passengerSearchValues = new PassengerSearchValues("passenger");
        this.mockMvc.perform(postJson("/passenger/search", passengerSearchValues))
                .andDo(print())
                .andExpect(status().isOk());
    }

}