package ru.DVorobiev.Taxi.agregator.util;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.ResultMatcher;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import ru.DVorobiev.Taxi.agregator.Entity.Address;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.header;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;

public class RequestBuilder {
    public static MockHttpServletRequestBuilder putJson(String uri, Object body) {
        try {
            String json = new ObjectMapper().writeValueAsString(body);
            return put(uri)
                    .contentType(MediaType.APPLICATION_JSON)
                    .accept(MediaType.APPLICATION_JSON)
                    .content(json);
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }
    }

    public static MockHttpServletRequestBuilder postJson(String uri, Object body) {
        try {
            String json = new ObjectMapper().writeValueAsString(body);
            return post(uri)
                    .contentType(MediaType.APPLICATION_JSON)
                    .accept(MediaType.APPLICATION_JSON)
                    .content(json);
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }
    }

    public ResultMatcher noCacheHeader() {
        return header().string("Cache-Control", "no-cache");
    }

    public static ResultMatcher address(String prefix, Address address) {
        return ResultMatcher.matchAll(
                jsonPath(prefix + ".id").value(address.getId()),
                jsonPath(prefix + ".city").value(address.getCity()),
                jsonPath(prefix + ".street").value(address.getStreet()),
                jsonPath(prefix + ".house").value(address.getHouse()),
                jsonPath(prefix + ".entrance").value(address.getEntrance()),
                jsonPath(prefix + ".coordinates").value(address.getCoordinates())
        );
    }

}
