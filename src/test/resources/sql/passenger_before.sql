DELETE FROM passenger;
LOCK TABLES passenger WRITE;
ALTER TABLE passenger DISABLE KEYS;

INSERT INTO passenger (name, phone, email, companion_сount) VALUES ('passenger Name 1','+7(342)-111-111-11','example@mail.ru',1);
INSERT INTO passenger (name, phone, email, companion_сount) VALUES ('passenger Name 2','+7(342)-111-111-11','example@mail.ru',1);
INSERT INTO passenger (name, phone, email, companion_сount) VALUES ('passenger Name 3','+7(342)-111-111-11','example@mail.ru',1);
INSERT INTO passenger (name, phone, email, companion_сount) VALUES ('passenger Name 4','+7(342)-111-111-11','example@mail.ru',1);
INSERT INTO passenger (name, phone, email, companion_сount) VALUES ('passenger Name 5','+7(342)-111-111-11','example@mail.ru',1);
INSERT INTO passenger (name, phone, email, companion_сount) VALUES ('passenger Name 6','+7(342)-111-111-11','example@mail.ru',1);
INSERT INTO passenger (name, phone, email, companion_сount) VALUES ('passenger Name 7','+7(342)-111-111-11','example@mail.ru',1);
INSERT INTO passenger (name, phone, email, companion_сount) VALUES ('passenger Name 8','+7(342)-111-111-11','example@mail.ru',1);
INSERT INTO passenger (name, phone, email, companion_сount) VALUES ('passenger Name 9','+7(342)-111-111-11','example@mail.ru',1);

ALTER TABLE passenger ENABLE KEYS;
UNLOCK TABLES;
