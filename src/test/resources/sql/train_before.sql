DELETE FROM train;
LOCK TABLES train WRITE;
ALTER TABLE train DISABLE KEYS;

INSERT INTO train (number, name, carriage, date) VALUES ('route_1','train_1',1,'2021-11-03 10:10');
INSERT INTO train (number, name, carriage, date) VALUES ('route_2','train_2',2,'2021-11-04 10:10');
INSERT INTO train (number, name, carriage, date) VALUES ('route_3','train_3',11,'2021-11-05 10:10');
INSERT INTO train (number, name, carriage, date) VALUES ('route_4','train_4',12,'2021-11-05 10:10');
INSERT INTO train (number, name, carriage, date) VALUES ('route_5','train_5',7,'2021-11-06 10:10');
INSERT INTO train (number, name, carriage, date) VALUES ('route_6','train_6',4,'2021-11-07 10:10');
INSERT INTO train (number, name, carriage, date) VALUES ('route_7','train_7',7,'2021-11-11 10:10');
INSERT INTO train (number, name, carriage, date) VALUES ('route_8','train_8',8,'2021-11-01 10:10');
INSERT INTO train (number, name, carriage, date) VALUES ('route_9','train_9',10,'2021-11-02 10:10');

ALTER TABLE train ENABLE KEYS;
UNLOCK TABLES;
