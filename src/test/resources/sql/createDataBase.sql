USE `taxi_test`;

DROP TABLE IF EXISTS trip;
DROP TABLE IF EXISTS passenger;
DROP TABLE IF EXISTS train;
DROP TABLE IF EXISTS `address`;

CREATE TABLE `address` (
                           `id` bigint NOT NULL AUTO_INCREMENT,
                           `city` varchar(45) NOT NULL,
                           `street` varchar(45) NOT NULL,
                           `house` integer NOT NULL,
                           `entrance` integer NOT NULL,
                           `coordinates` varchar(20) NOT NULL,
                           PRIMARY KEY (`id`),
                           KEY `index_title` (`city`) /*!80000 INVISIBLE */
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;


CREATE TABLE `train` (
                         `id` bigint NOT NULL AUTO_INCREMENT,
                         `number` varchar(15) NOT NULL,
                         `name` varchar(45) NOT NULL,
                         `carriage` integer NOT NULL,
                         `date` datetime DEFAULT NULL,
                         PRIMARY KEY (`id`),
                         KEY `index_title` (`number`) /*!80000 INVISIBLE */
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `passenger` (
                             `id` bigint NOT NULL AUTO_INCREMENT,
                             `name` varchar(30) NOT NULL,
                             `phone` varchar(20) NOT NULL,
                             `email` varchar(30) NOT NULL,
                             `companion_сount` integer NOT NULL,
                             PRIMARY KEY (`id`),
                             KEY `name` (`name`) /*!80000 INVISIBLE */
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `trip` (
                        `id` bigint NOT NULL AUTO_INCREMENT,
                        `name` varchar(45) NOT NULL,
                        `price` float NOT NULL,
                        `comment` varchar(30),
                        `address_id` bigint DEFAULT NULL,
                        `train_id` bigint DEFAULT NULL,
                        `passenger_id` bigint DEFAULT NULL,
                        PRIMARY KEY (`id`),
                        KEY `fk_address_idx` (`address_id`),
                        KEY `fk_train_idx` (`train_id`),
                        KEY `fk_passenger_idx` (`passenger_id`),
                        KEY `index_name` (`name`),
                        KEY `index_price` (`price`),
                        CONSTRAINT `fk_address` FOREIGN KEY (`address_id`) REFERENCES `address` (`id`) ON DELETE SET NULL ON UPDATE RESTRICT,
                        CONSTRAINT `fk_train` FOREIGN KEY (`train_id`) REFERENCES `train` (`id`) ON DELETE SET NULL ON UPDATE RESTRICT,
                        CONSTRAINT `fk_passenger` FOREIGN KEY (`passenger_id`) REFERENCES `passenger` (`id`) ON DELETE SET NULL ON UPDATE RESTRICT
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;