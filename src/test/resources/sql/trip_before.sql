DELETE FROM trip;
LOCK TABLES trip WRITE;
ALTER TABLE trip DISABLE KEYS;

INSERT INTO trip (name, price, comment, address_id, train_id, passenger_id) VALUES ('Trip 1',110.0,'comment 1',1,1,1);
INSERT INTO trip (name, price, comment, address_id, train_id, passenger_id) VALUES ('Trip 2',150.0,'comment 2',2,2,2);
INSERT INTO trip (name, price, comment, address_id, train_id, passenger_id) VALUES ('Trip 3',180.0,'comment 3',3,3,3);
INSERT INTO trip (name, price, comment, address_id, train_id, passenger_id) VALUES ('Trip 4',200.0,'comment 4',4,4,4);
INSERT INTO trip (name, price, comment, address_id, train_id, passenger_id) VALUES ('Trip 5',800.0,'comment 5',5,5,5);
INSERT INTO trip (name, price, comment, address_id, train_id, passenger_id) VALUES ('Trip 6',1100.0,'comment 6',6,6,6);
INSERT INTO trip (name, price, comment, address_id, train_id, passenger_id) VALUES ('Trip 7',1200.0,'comment 7',7,7,7);
INSERT INTO trip (name, price, comment, address_id, train_id, passenger_id) VALUES ('Trip 8',9900.0,'comment 8',8,8,8);
INSERT INTO trip (name, price, comment, address_id, train_id, passenger_id) VALUES ('Trip 9',1000.0,'comment 9',9,9,9);

ALTER TABLE trip ENABLE KEYS;
UNLOCK TABLES;
